#!/bin/bash
#
# Usage:
#
#   ./hack/update-env.sh envs python3.9

set -eo pipefail

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

envPath="$1"
envName="$2"

shift 2

function cleanup() {
  if [ "$PWD" == "$tmp" ]; then
    popd
  fi
  rm -rf "$tmp"
}

trap cleanup EXIT SIGINT

tmp="$(mktemp -d "$envName.XXXXX")"

envFile="$envPath/$envName.yaml" 
if [ ! -f "$envFile" ]; then
  echo "No construct yaml for $envName"
  exit 1
fi

# TODO: if more files are added to env path to be copied, the can be added 
#       to the cp.
cp "$envFile" "$tmp"/construct.yaml
( 
  cd "$envPath"; cp eula.txt "$tmp" 
)

outputDir="$SCRIPT_ROOT"/out
if [ ! -d "$outputDir" ]; then
  mkdir -v "$outputDir"
fi

constructor --output-dir "$outputDir" "$tmp"
