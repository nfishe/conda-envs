ENVS_PATH = envs

vpath %.yaml $(ENVS_PATH)

%: %.yaml
	./hack/update-env.sh $(ENVS_PATH) $*